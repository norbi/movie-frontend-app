import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { useParams } from 'react-router-dom';
import axios from 'axios';
import { Grid, List } from '@material-ui/core';
import ErrorMessage from './ErrorMessage';
import LoadingIndicator from './LoadingIndicator';
import { makeStyles } from '@material-ui/core/styles';
import RelatedResult from './RelatedResult';
const IMDB_API_KEY = `${process.env.REACT_APP_IMDB_API_KEY}`;

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    marginTop: '25px'
  },
}));

const RelatedMovies = props => {
  const classes = useStyles();
  const { imDbId } = useParams();
  const [movies, setMovies] = useState([]);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(false);

  useEffect(() => {
    if (imDbId === null) {
      return
    };
    fetchRelatedMovies(imDbId);
    }, [imDbId],
  );

  const fetchRelatedMovies = (id) => {
    axios.get(`https://imdb-api.com/en/API/Title/${IMDB_API_KEY}/${id}/Wikipedia`).then(response => {
      setMovies(response?.data?.similars);
    }).catch(() => {
      setError(true);
    }).then(() => {
      setLoading(false);
    });
  }

  return (
    <div className={classes.root}>
      <Grid container spacing={3} justify="center">
        <Grid item xs={10}>
          { error && <ErrorMessage /> }
          <List dense>
            {
              movies.map(
                (movie) => (<RelatedResult movie={movie} />)
              )
            }
          </List>
        </Grid>
      </Grid>
      <LoadingIndicator open={loading} />
    </div>
  );
};

export default RelatedMovies;
