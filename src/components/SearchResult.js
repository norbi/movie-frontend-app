import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { CircularProgress, IconButton, ListItem, ListItemSecondaryAction, ListItemText } from '@material-ui/core';
import { Info as InfoIcon } from '@material-ui/icons';
import axios from 'axios';
import DOMPurify from "dompurify";
import parse from "html-react-parser";
import { makeStyles } from '@material-ui/core/styles';
import { Link } from 'react-router-dom';

const useStyles = makeStyles((theme) => ({
  listItem: {
    borderBottom: '1px solid lightgray'
  },
  externalLink: {
    marginRight: '5px'
  }
}));

const SearchResult = props => {
  const { result } = props;
  const classes = useStyles();
  const [details, setDetails] = useState('');
  const [wikipediaLink, setWikipediaLink] = useState('');
  const [imdbLink, setImdbLink] = useState('');
  const [isLoading, setIsLoading] = useState(false);
  const [imDbId, setImDbId] = useState(null);
  const IMDB_API_KEY = `${process.env.REACT_APP_IMDB_API_KEY}`;

  const fetchWikipediaData = (imdbId) => {
    axios.get(`https://imdb-api.com/en/API/Title/${IMDB_API_KEY}/${imdbId}/Wikipedia`).then(response => {
      const { data } = response;
      const cleanHtmlDetails = DOMPurify.sanitize(data?.wikipedia?.plotShort?.html, {
        USE_PROFILES: { html: true },
      });
      setDetails(cleanHtmlDetails);
      setWikipediaLink(data?.wikipedia?.url);
    });
  };

  const fetchExternalSites = (imdbId) => {
    axios.get(`https://imdb-api.com/en/API/ExternalSites/${IMDB_API_KEY}/${imdbId}`).then(response => {
      if (response.data?.imDb?.url !== undefined) {
        setImdbLink(response.data?.imDb?.url);
      }
    });
  };

  const fetchDetails = (e, title) => {
    e.preventDefault();
    setIsLoading(true)

    axios.get(`https://imdb-api.com/en/API/SearchTitle/${IMDB_API_KEY}/${title}`).then(response => {
      const { results } = response.data;
      if (results !== null) {
        setImDbId(results[0]?.id);
      }
    }).catch(() => {
      setDetails('Something went wrong!');
      setIsLoading(false);
    });
  }

  useEffect(() => {
    if (imDbId === null) {
      return null;
    }

    setIsLoading(true);
    Promise.all([fetchWikipediaData(imDbId), fetchExternalSites(imDbId)]).then(results => {
      setIsLoading(false);
    }).catch(() => {
      setIsLoading(false);
      setDetails('Something went wrong!');
    });
    }, [imDbId],
  );

  return (
    <ListItem className={classes.listItem}>
      <ListItemText
        primary={result.name}
        secondary={
          <>
            <div>
              {result.overview}
            </div>
            <div>
              {isLoading && <CircularProgress /> }
              {parse(details)}
            </div>
            <div>
              {imdbLink.length > 0 && <a href={imdbLink} target="_blank" className={classes.externalLink}>Imdb</a>}
              {wikipediaLink.length > 0 && <a href={wikipediaLink} target="_blank" className={classes.externalLink}>Wikipedia</a>}
              {imDbId && <Link to={`/movie/${imDbId}/related-movies`} className={classes.externalLink}>Related movies</Link>}
            </div>
          </>
        }
      />
      {
        details.length === 0
        && <ListItemSecondaryAction >
            <IconButton edge="end" aria-label="delete" onClick={e => fetchDetails(e, result.name)}>
              <InfoIcon />
            </IconButton>
          </ListItemSecondaryAction>
      }

    </ListItem>
  );
}

SearchResult.propTypes = {
  result: PropTypes.shape({
    id: PropTypes.string,
    name: PropTypes.string,
    overview: PropTypes.string,
    releaseDate: PropTypes.string,
  }).isRequired
};

export default SearchResult;
