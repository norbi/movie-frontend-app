import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { TextField } from '@material-ui/core';

const SearchBar = props => {
  const {
    label, searchCallback,
  } = props;

  const [searchValue, setSearchValue] = useState('')

  const onChangeHandler = e => {
    e.preventDefault();
    setSearchValue(e.target.value);
  }

  const submitHandler = e => {
    e.preventDefault();
    searchCallback(searchValue);
    setSearchValue('')
  }

  return (
    <form noValidate onSubmit={submitHandler}>
      <TextField fullWidth label={label} onChange={onChangeHandler} />
    </form>

  )
};

SearchBar.defaultProps = {
  label: '',
  searchCallback() {}
}
SearchBar.propTypes = {
  label: PropTypes.string,
  searchCallback: PropTypes.func,
};

export default SearchBar;
