import React from 'react';
import PropTypes from 'prop-types';

const ErrorMessage = props => {
  const { message } = props;

  return (
    <div>
      Something went wrong! {message}
    </div>
  );
};

ErrorMessage.defaultProps = {
  message: ''
}

ErrorMessage.propTypes = {
  message: PropTypes.string
};

export default ErrorMessage;
