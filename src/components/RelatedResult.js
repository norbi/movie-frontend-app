import React from 'react';
import PropTypes from 'prop-types';
import { ListItem, ListItemText } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  listItem: {
    borderBottom: '1px solid lightgray'
  }
}));

const RelatedResult = props => {
  const classes = useStyles();
  const { movie } = props;

  return (
    <ListItem className={classes.listItem}>
      <ListItemText
        primary={movie.title}
        secondary={movie.plot}
      />
    </ListItem>
  );
};

RelatedResult.propTypes = {
  movie: PropTypes.shape({
    id: PropTypes.string,
    title: PropTypes.string,
    fullTitle: PropTypes.string,
    plot: PropTypes.string,
    year: PropTypes.string,
    stars: PropTypes.string,
    image: PropTypes.string,
    genres: PropTypes.string,
    directors: PropTypes.string,
  })
};

export default RelatedResult;
