import React from 'react';
import { Grid, List } from '@material-ui/core';
import SearchBar from './SearchBar';
import ErrorMessage from './ErrorMessage';
import SearchResult from './SearchResult';
import LoadingIndicator from './LoadingIndicator';
import { makeStyles } from '@material-ui/core/styles';
import { gql, useLazyQuery } from '@apollo/client';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    marginTop: '25px'
  },
}));

const MovieSearch = props => {
  const classes = useStyles();

  const SEARCH_MOVIES = gql`
      query SearchMovies($query: String!) {
          searchMovies(query: $query) {
              id
              name
              overview
              releaseDate

          }
      }
  `;
  const [fetchMovies, { loading, error, data }] = useLazyQuery(SEARCH_MOVIES);

  const searchMovies = (queryString) => {
    fetchMovies({ variables: { query: queryString } })
  }
  return (
    <div className={classes.root}>
      <Grid container spacing={3} justify="center">
        <Grid item xs={10}>
          <SearchBar label="Movies" searchCallback={searchMovies} />
          { error && <ErrorMessage message={error.message} /> }
          <List dense>
            {
              data?.searchMovies.map(
                (result, index) => <SearchResult key={`${index}-${result.id}`} result={result} />
              )
            }
          </List>
        </Grid>
      </Grid>
      <LoadingIndicator open={loading} />
    </div>
  );
};

export default MovieSearch;
