import React from 'react'
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";

import MovieSearch from './components/MovieSearch';
import RelatedMovies from './components/RelatedMovies';

export default function App() {

  return (
    <Router>
      <Switch>
        <Route path="/movie/:imDbId/related-movies">
          <RelatedMovies />
        </Route>
        <Route path="/">
          <MovieSearch />
        </Route>
      </Switch>
    </Router>
  );
}
